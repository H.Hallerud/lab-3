package no.uib.inf101.terminal;

public class CmdPwd implements Command{
    Context context = new Context(); 
    
    @Override
    public void setContext(Context context){
        Command.super.setContext(context);
    }

    @Override
    public String run(String[] args){
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName(){
        return "pwd";
    }
}
