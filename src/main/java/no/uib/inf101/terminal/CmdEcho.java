package no.uib.inf101.terminal;

public class CmdEcho implements Command{
    @Override
    public String run(String[] args){
        String allEcho = "";
        for (String s : args) {
            allEcho += (s + " ");
        }
        return allEcho;
    }

    @Override
    public String getName(){
        return "echo";
    }
}
